<?php
namespace Exc\CommandBundle\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Exc\CommandBundle\Entity\TimeOff;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exc\CommandBundle\Mailer\Mailer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendEmailListener
{
    /**
     * @var \Swift_Mailer $mailer
     */
    protected $mailer;

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $this->mailer = $this->container->get('exc_command.mailer.mailer');

        $entity = $event->getEntity();

        if ($entity instanceof TimeOff) {

            if ($event->getOldValue('status') != $entity->getStatus()) {

                if ($entity->getStatus() == TimeOff::STATUS_APPROVED || $entity->getStatus() == TimeOff::STATUS_NOT_APPROVED) {

                    $managerName = $entity->getManager()->getEnName();

                    $this->mailer->sendEmailForEmployee($entity, $managerName);
                }
            }
        }
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
        $this->mailer = $this->container->get('exc_command.mailer.mailer');

        $entity = $event->getEntity();

        if ($entity instanceof TimeOff) {

            $this->mailer->sendEmailForManager($entity, $entity->getEmployee()->getEnName());
        }
    }
}