<?php
namespace Exc\CommandBundle\Controller;

use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Entity\TimeOff;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CRUDController extends Controller
{

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function declinedAction($id) {

        $request = Request::createFromGlobals();

        $comment = $request->query->get('comment');

        if (!$id) {
            throw new NotFoundHttpException(sprintf('unable to find the id : %s', $id));
        }

        $em = $this->getDoctrine()->getManager();

        $timeOff = $em->getRepository('ExcCommandBundle:TimeOff')->find($id);

        $timeOff->setManager($this->getUser());

        $timeOff->setStatus(TimeOff::STATUS_NOT_APPROVED);

        $timeOff->setComment($comment);

        $em->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function approvedAction($id) {

        if (!$id) {
            throw new NotFoundHttpException(sprintf('unable to find the id : %s', $id));
        }

        $em = $this->getDoctrine()->getManager();

        $timeOff = $em->getRepository('ExcCommandBundle:TimeOff')->find($id);

        $timeOff->setManager($this->getUser());

        $timeOff->setStatus(TimeOff::STATUS_APPROVED);

        $em->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}