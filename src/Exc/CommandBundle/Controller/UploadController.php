<?php
namespace Exc\CommandBundle\Controller;

use Exc\CommandBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CoreController;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class UploadController extends CoreController
{
    /**
     * @param Request $request
     * @param string $type
     * @return Response|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function uploadAction(Request $request, $type)
    {
        $type = $type != User::MONTHLY_REPORT ? User::WEEKLY_REPORT : User::MONTHLY_REPORT;

        $form = $this->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $uploadFile = $form->getData();
                /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadFile */
                $uploadFile = $uploadFile['file'];
                $fileName = $uploadFile->getPathname();

                return $this->getExcelFile($fileName, $type);
            }
        }

        return $this->render('ExcCommandBundle:Upload:upload.html.twig', array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'type' => $type,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $fileName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    protected function getExcelFile($fileName, $type)
    {
        $response = false;
        $excBuilder = $this->get('exc_command.builder.excel_builder');
        $csvParser = $this->get('exc_command.parser.csv_parser');
        $employeesData = $this->getDoctrine()->getRepository('ExcCommandBundle:Employee')->findAll();

        switch ($type) {
            case User::WEEKLY_REPORT:
                $employeesData = $excBuilder->initEmployeesWeeklyData($employeesData);
                $employeesData = $csvParser->parseWeeklyCsv($fileName, $employeesData);
                $newXls = $excBuilder->setEmployeesWeeklyData($employeesData);
                $response = $excBuilder->getExcelResponse($newXls, $type);
                break;
            case User::MONTHLY_REPORT:
                $employeesData = $excBuilder->initEmployeesMonthlyData($employeesData);
                $employeesData = $csvParser->parseMonthlyCsv($fileName, $employeesData);
                $newXls = $excBuilder->setEmployeesMonthlyData($employeesData);
                $response = $excBuilder->getExcelResponse($newXls, $type);
                break;
        }

        return $response;
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getForm()
    {
        return $this->createFormBuilder()
            ->add('file', 'file', array(
                'required' => false,
                'constraints' => array(
                    new File(array(
                        'mimeTypes' => array(
                            'text/plain',
                            'text/csv',
                        ),
                        'mimeTypesMessage' => 'Type of the file is invalid.  Allowed types "csv/txt"',
                        'maxSize' => '5000k',
                    )),
                    new NotBlank(),
                ),
            ))
            ->add('Upload', 'submit', array(
                'attr' => array(
                    'class' => 'btn btn-sm btn-success',
                ),
            ))
            ->getForm()
        ;
    }

    /**
     * @param $fileName
     * @return string
     */
    protected function getPathToCsvFile($fileName)
    {
        return 'upload/' .  $fileName;
    }
} 