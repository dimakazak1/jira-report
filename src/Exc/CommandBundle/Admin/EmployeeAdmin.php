<?php
namespace Exc\CommandBundle\Admin;

use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Entity\Rate;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Admin\AdminInterface;

class EmployeeAdmin extends Admin
{
    protected $encoderFactory;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param string $encoderFactory
     */
    public function __construct($code, $class, $baseControllerName, $encoderFactory)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('enName', null, array('label' => 'Фамилия на английском', 'required' => false))
            ->add('ruName', null, array('label' => 'Фамилия на русском', 'required' => false))
            ->add('email', null, array('label' => 'Email', 'required' => false))
            ->add('plainPassword', 'password', array('label' => 'Password', 'required' => false))
            ->add('startTestPeriod', 'date', array(
                    'required' => false,
                    'label' => 'Начало тестового периода',
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                )
            )->add('endTestPeriod', 'date', array(
                    'required' => false,
                    'label' => 'Конец тестового периода',
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                )
            )
            ->add('rate', 'entity', array(
                    'required' => true,
                    'label' => 'Ставка',
                    'class' => 'ExcCommandBundle:Rate',
                    'property' => 'title',
                )
            )->add('firedDate', 'date', array(
                    'required' => false,
                    'label' => 'Уволен',
                    'input'  => 'datetime',
                    'widget' => 'single_text'
                )
            )
            ->add('isManager', null, array('label' => 'Менеджер', 'required' => false));
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object) {

        if ($object instanceof Employee) {

            if ($object->getPlainPassword()) {
                $employee = $object;

                $factory = $this->encoderFactory;
                $encoder = $factory->getEncoder($employee);
                $password = $encoder->encodePassword($object->getPlainPassword(), $employee->getSalt());
                $employee->setPassword($password);
            }
        }
    }

    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        if ($object instanceof Employee) {

            if ($object->getPlainPassword()) {
                $employee = $object;

                $factory = $this->encoderFactory;
                $encoder = $factory->getEncoder($employee);
                $password = $encoder->encodePassword($object->getPlainPassword(), $employee->getSalt());
                $employee->setPassword($password);
            }
        }
    }

    /**
     * @param FormMapper $showMapper
     */
    protected function configureShowField(FormMapper $showMapper)
    {
        $showMapper
            ->add('enName', null, array('label' => 'Фамилия на английском'))
            ->add('ruName', null, array('label' => 'Фамилия на русском'))
            ->add('isManager', 'boolean', array('label' => 'Менеджер'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('ruName', null, array('label' => 'Фамилия на русском'))
            ->add('enName', null, array('label' => 'Фамилия на английском'))
            ->add('email', null, array('label' => 'Email'))
            ->add('isManager', 'boolean', array('label' => 'Менеджер'))
            ->add('rate.title', null, array('label' => 'Ставка'))
            ->add('_action', 'actions',  array(
                    'label' => 'Действия',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                )
            );
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('ruName', null, array('label' => 'Фамилия на русском'))
            ->add('enName', null, array('label' => 'Фамилия на aнглийском'));
    }
}