<?php
namespace Exc\CommandBundle\Admin\Account;

use Exc\CommandBundle\Entity\TimeOff;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class TimeOffAdmin
 * @package Exc\CommandBundle\Admin\Account
 */
class TimeOffAdmin extends Admin
{
    /**
     * @param mixed $object
     */
    public function prePersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        if (!$container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null !== $token = $container->get('security.context')->getToken()) {
            if (is_object($user = $token->getUser())) {
                $object->setEmployee($user);
                $object->setManager($user);
            }
        }
    }

    /**
     * @param mixed $object
     */
    public function preUpdate($object) {

        $container = $this->getConfigurationPool()->getContainer();

        if (!$container->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null !== $token = $container->get('security.context')->getToken()) {
            if (is_object($user = $token->getUser())) {
                $object->setManager($user);
            }
        }
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /**
         * Create block
        */
        if($this->getRequest()->get($this->getIdParameter()) == null)
        {
            $formMapper
                ->add('status', 'choice', array(
                        'required' => false,
                        'choices' => array(
                            TimeOff::STATUS_PENDING => 'Pending',
                            TimeOff::STATUS_APPROVED => 'Approved',
                            TimeOff::STATUS_NOT_APPROVED => 'Declined',
                        ),
                    )
                )
                ->add('type', 'entity', array(
                        'required' => true,
                        'class' => 'ExcCommandBundle:TypeTimeOff',
                        'property' => 'title',
                    )
                )
                ->add('startDate', 'date', array(
                        'required' => false,
                        'label' => 'Start Date',
                        'input'  => 'datetime',
                        'widget' => 'single_text',
                        'data' => new \DateTime(),
                    )
                )
                ->add('endDate', 'date', array(
                        'required' => false,
                        'label' => 'End Date',
                        'input'  => 'datetime',
                        'widget' => 'single_text',
                        'data' => new \DateTime('tomorrow '),
                    )
                )
                ->add('reason', null, array(
                        'required' => false,
                    )
                )
                ->add('comment', null, array(
                        'required' => false,
                    )
                );

        } else {
            /**
             * Update block
             */
            $formMapper
                ->add('status', 'choice', array(
                        'required' => false,
                        'choices' => array(
                            TimeOff::STATUS_PENDING => 'Pending',
                            TimeOff::STATUS_APPROVED => 'Approved',
                            TimeOff::STATUS_NOT_APPROVED => 'Declined',
                        ),
                    )
                )
                ->add('type', 'entity', array(
                        'required' => true,
                        'class' => 'ExcCommandBundle:TypeTimeOff',
                        'query_builder' => function(EntityRepository $repository) {
                            /** @var  */
                            return $repository->createQueryBuilder('t')->orderBy('t.id', 'ASC');
                        },
                        'property' => 'title',
                    )
                )
                ->add('comment', null, array(
                        'required' => false,
                    )
                );

        }
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('employee', 'sonata_type_model')
            ->add('type', null)
            ->add('reason', null)
            ->add('startDate', 'date')
            ->add('endDate', 'date')
            ->add('status', 'choice',
                array('choices' => array(
                        TimeOff::STATUS_PENDING => 'Pending',
                        TimeOff::STATUS_APPROVED => 'Approved',
                        TimeOff::STATUS_NOT_APPROVED => 'Declined',
                    ),
                )
            )
            ->add('_action', 'actions',  array(
                'label' => 'Actions',
                'actions' => array(
                    'edit' => array(),
                    'Approved' => array(
                        'template' => 'ExcCommandBundle:CRUD:list__action_approved.html.twig'
                    ),
                    'Declined' => array(
                        'template' => 'ExcCommandBundle:CRUD:list__action_decline.html.twig'
                    ),
                )
            ))
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('employee', null, array('label' => 'Employee'))
            ->add('status', 'doctrine_orm_choice', array('label' => 'Status',
                'field_options' => array(
                    'required' => false,
                    'choices' => array(
                        TimeOff::STATUS_PENDING => 'Pending',
                        TimeOff::STATUS_APPROVED => 'Approved',
                        TimeOff::STATUS_NOT_APPROVED => 'Declined',
                    )
                ),
                'field_type' => 'choice'
            ));
    }


    /**
     * @return array
     */
    public function getFilterParameters()
    {
        $default = [
            'status' => ['value' => TimeOff::STATUS_PENDING],
        ];

        $this->datagridValues = array_merge($default, $this->datagridValues);

        return parent::getFilterParameters();
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('approved', $this->getRouterIdParameter().'/approved');
        $collection->add('declined', $this->getRouterIdParameter().'/declined');
    }
} 