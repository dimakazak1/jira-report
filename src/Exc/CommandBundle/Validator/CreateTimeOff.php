<?php
namespace Exc\CommandBundle\Validator;

use Symfony\Component\Validator\Constraint;


class CreateTimeOff extends Constraint
{
    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'create_time_off';
    }

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}