<?php
namespace Exc\CommandBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Exc\CommandBundle\Entity\TypeTimeOff;
use Exc\CommandBundle\Validator\Helper\Createtimeoff\CreateTimeOffValidatorHelper;
use Exc\CommandBundle\Entity\TimeOff;

class CreateTimeOffValidator extends ConstraintValidator
{
    /**
     * @var Helper\Createtimeoff\CreateTimeOffValidatorHelper
     */
    protected $helper;

    /**
     * @param CreateTimeOffValidatorHelper $helper
     */
    public function __construct(CreateTimeOffValidatorHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param TimeOff $timeOff
     * @param Constraint $constraint
     * @return bool
     */
    public function validate($timeOff, Constraint $constraint)
    {
        if (null == $timeOff->getType()) {
            $type = '';
        } else {
            $type = $timeOff->getType()->getKey();
        }

        switch ($type) {

            case TypeTimeOff::UNPAID_LEAVE:
                if (false == $this->helper->checkUnpaidLeave($timeOff)) {
                    $this->context->addViolationAt(null, 'Неоплачиваемый отпуск не может быть дольше 60 дней.');
                }
                break;

            case TypeTimeOff::TRANSFER:
                if (false == $this->helper->checkTransfer($timeOff)) {
                    $this->context->addViolationAt(null, 'Перенос даты возможен не беолее чем на 2 месяца.');
                }
                break;

            case TypeTimeOff::PAID_TIME_OFF:
                if (false == $this->helper->checkPaid($timeOff)) {
                    $this->context->addViolationAt(null, 'Оплачиваемых отгулов не может быть больше 5 в году.');
                }
                break;

            case TypeTimeOff::VACATION:
                if (false === $this->helper->checkVacation($timeOff)) {
                    $this->context->addViolationAt(null, 'Отпуск не может быть дольше 30 дней.');
                }
                break;

            case TypeTimeOff::SICK_LEAVE:
                if (false === $this->helper->checkSick($timeOff)) {
                    $this->context->addViolationAt(null, 'Больничный не может быть дольше 30 дней.');
                }
                break;

            case TypeTimeOff::RECRUITMENT_OFFICE:
                if (false === $this->helper->checkRecruitment($timeOff)) {
                    $this->context->addViolationAt(null, 'Максимальное количество дней для военкомата 30.');
                }
                break;
        }
    }
}