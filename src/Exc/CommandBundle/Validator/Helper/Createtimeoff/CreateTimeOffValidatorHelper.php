<?php
namespace Exc\CommandBundle\Validator\Helper\Createtimeoff;

use Exc\CommandBundle\Entity\TimeOff;

class CreateTimeOffValidatorHelper
{
    /**
     * @var integer
     */
    protected $countDaysPaidTimeOff;

    /**
     * @var integer
     */
    protected $minCountUnpaidLeave;

    /**
     * @var integer
     */
    protected $maxCountUnpaidLeave;

    /**
     * @var integer
     */
    protected $minCountTransfer;

    /**
     * @var integer
     */
    protected $maxCountTransfer;

    /**
     * @var integer
     */
    protected $differenceTransfer;

    /**
     * @var integer
     */
    protected $minCountPaidTimeOff;

    /**
     * @var integer
     */
    protected $maxCountPaidTimeOff;

    /**
     * @var integer
     */
    protected $minCountVacation;

    /**
     * @var integer
     */
    protected $maxCountVacation;

    /**
     * @var integer
     */
    protected $minCountSickLeave;

    /**
     * @var integer
     */
    protected $maxCountSickLeave;

    /**
     * @var integer
     */
    protected $minCountRecruitment;

    /**
     * @var integer
     */
    protected $maxCountRecruitment;

    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    public function __construct(
                                \Doctrine\ORM\EntityManager $em,
                                $countDaysPaidTimeOff,
                                $minCountUnpaidLeave,
                                $maxCountUnpaidLeave,
                                $minCountTransfer,
                                $maxCountTransfer,
                                $differenceTransfer,
                                $minCountPaidTimeOff,
                                $maxCountPaidTimeOff,
                                $minCountVacation,
                                $maxCountVacation,
                                $minCountSickLeave,
                                $maxCountSickLeave,
                                $minCountRecruitment,
                                $maxCountRecruitment
                                )
    {
        $this->em = $em;
        $this->countDaysPaidTimeOff = $countDaysPaidTimeOff;
        $this->minCountUnpaidLeave = $minCountUnpaidLeave;
        $this->maxCountUnpaidLeave = $maxCountUnpaidLeave;
        $this->minCountTransfer = $minCountTransfer;
        $this->maxCountTransfer = $maxCountTransfer;
        $this->differenceTransfer = $differenceTransfer;
        $this->minCountPaidTimeOff = $minCountPaidTimeOff;
        $this->maxCountPaidTimeOff = $maxCountPaidTimeOff;
        $this->minCountVacation = $minCountVacation;
        $this->maxCountVacation = $maxCountVacation;
        $this->minCountSickLeave = $minCountSickLeave;
        $this->maxCountSickLeave = $maxCountSickLeave;
        $this->minCountRecruitment = $minCountRecruitment;
        $this->maxCountRecruitment = $maxCountRecruitment;
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkUnpaidLeave(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        $difference = $startDate->diff($endDate);

        if (($difference->days >= $this->minCountUnpaidLeave && $difference->days <= $this->maxCountUnpaidLeave) && $startDate < $endDate) {
            return true;
        }

        return false;
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkTransfer(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();

        $difference = $startDate->diff($endDate);

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        if (($difference->days >= $this->minCountTransfer && $difference->days <= $this->maxCountTransfer) && $startDate < $endDate) {
            $date = new \DateTime();
            $fromToDifference = $startDate->diff($date);
            if ($fromToDifference->days <= $this->differenceTransfer ) {
                return true;
            }
        }
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkPaid(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();
        $difference = $startDate->diff($endDate);

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        $date = new \DateTime();
        $date->setDate(Date('Y'), 1, 1);

        $count = $this->em->getRepository('ExcCommandBundle:TimeOff')->getCountUserPaidTimeOff($date, $timeOff);

        if ($count < $this->countDaysPaidTimeOff) {
            if (($difference->days >= $this->minCountPaidTimeOff && $difference->days <= $this->maxCountPaidTimeOff) && $startDate < $endDate) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkVacation(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        $difference = $startDate->diff($endDate);

        if (($difference->days >= $this->minCountVacation && $difference->days <= $this->maxCountVacation) && $startDate < $endDate) {
            return true;
        }

        return false;
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkSick(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        $difference = $startDate->diff($endDate);

        if (($difference->days >= $this->minCountSickLeave && $difference->days <= $this->maxCountSickLeave) && $startDate < $endDate) {
            return true;
        }

        return false;
    }

    /**
     * @param TimeOff $timeOff
     * @return bool
     */
    public function checkRecruitment(TimeOff $timeOff)
    {
        $startDate = $timeOff->getStartDate();
        $endDate = $timeOff->getEndDate();

        if (!$this->checkEmpty($startDate, $endDate)) {
            return false;
        }

        $difference = $startDate->diff($endDate);

        if (($difference->days >= $this->minCountRecruitment && $difference->days <= $this->maxCountRecruitment) && $startDate < $endDate) {
            return true;
        }

        return false;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return bool
     */
    protected function checkEmpty(\DateTime $startDate, \DateTime $endDate)
    {
        return !(empty($startDate) || empty($endDate));
    }
}