<?php
namespace Exc\CommandBundle\Mailer;

use \Doctrine\ORM\EntityManager;
use \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Exc\CommandBundle\Entity\TimeOff;
use Exc\CommandBundle\Entity\Employee;

class Mailer
{
    /**
     * @var \Swift_Mailer $mailer
     */
    protected $mailer;

    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    /**
     * @var \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine $templating
     */
    protected $templating;

    /**
     * @var string $from
     */
    protected $from = 'holiday@gmail.by';

    /**
     * @var string $subject
     */
    protected $subject = 'Time Off';

    /**
     * @param \Swift_Mailer $mailer
     * @param EntityManager $em
     * @param TimedTwigEngine $templating
     */
    public function __construct(\Swift_Mailer $mailer, EntityManager $em, TimedTwigEngine $templating)
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->templating = $templating;
    }

    /**
     * @param  string $email
     * @param string $link
     */
    public function sendResetPassEmail($email, $link)
    {
        $this->mailer->send($this->getResetPassMessage($email, $link));
    }

    /**
     * @param TimeOff $timeOff
     * @param $managerName
     */
    public function sendEmailForEmployee(TimeOff $timeOff, $managerName)
    {
        $this->mailer->send($this->getMessageForEmployee($timeOff, $managerName));
    }

    /**
     * @param TimeOff $timeOff
     * @param $userName
     */
    public function sendEmailForManager(TimeOff $timeOff, $userName)
    {
        $managers = $this->em->getRepository('ExcCommandBundle:Employee')->findAllManagers();

        foreach ($managers as $manager) {
            $this->mailer->send($this->getMessageForManager($manager, $timeOff, $userName));
        }
    }

    /**
     * @param string $email
     * @param string $link
     * @return \Swift_Mime_MimePart
     */
    protected function getResetPassMessage($email, $link)
    {
        return \Swift_Message::newInstance()
            ->setSubject('Reset Password')
            ->setFrom($this->from)
            ->setTo($email)
            ->setBody(
                $this->templating->render(
                    'ExcExcelBundle:Email:email_reset_password.html.twig',
                    array('link' => $link)
                )
            );
    }

    /**
     * @param Employee $manager
     * @param TimeOff $timeOff
     * @param $userName
     * @return \Swift_Mime_MimePart
     */
    protected function getMessageForManager(Employee $manager, TimeOff $timeOff, $userName)
    {
        return \Swift_Message::newInstance()
            ->setSubject($this->subject)
            ->setFrom($this->from)
            ->setTo($manager->getEmail())
            ->setBody(
                $this->templating->render(
                    'ExcExcelBundle:Email:email_for_managers.html.twig',
                    array('timeOff' => $timeOff, 'userName' => $userName)
                )
            );
    }
    /**
     * @param TimeOff $timeOff
     * @param $mangerName
     * @return \Swift_Mime_MimePart
     */
    protected function getMessageForEmployee(TimeOff $timeOff, $mangerName)
    {
        return \Swift_Message::newInstance()
            ->setSubject($this->subject)
            ->setFrom($this->from)
            ->setTo($timeOff->getEmployee()->getEmail())
            ->setBody(
                $this->templating->render(
                    'ExcExcelBundle:Email:email_for_employee.html.twig',
                    array('timeOff' => $timeOff, 'managerName' => $mangerName)
                )
            );
    }
} 