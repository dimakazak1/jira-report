<?php
namespace Exc\CommandBundle\Builder;

use Exc\CommandBundle\Entity\User;
use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Entity\TypeTimeOff;
use Exc\CommandBundle\Parser\CalendarParser;
use Liuggio\ExcelBundle\Factory;
use \Doctrine\ORM\EntityManager;

class ExcelBuilder
{
    const MONTHLY_ALL_TIME_INDEX = 'allTime';
    const MONTHLY_TIME_BY_DATE_INDEX = 'timeByDate';

    const AUTHOR_CELL = 'A';
    const TIME_CELL = 'B';

    /**
     * @var \Liuggio\ExcelBundle\Factory
     */
    protected $phpExcel;

    protected $em;

    protected $calendar;

    protected $weeklyDays = [];

    protected $coordinator = 'Кот';

    protected $countDaysPaidTimeOff;

    /**
     * @param Factory $phpExcel
     * @param EntityManager $em
     * @param $calendar
     * @param $countDaysPaidTimeOff
     */
    public function __construct(Factory $phpExcel, EntityManager $em, $calendar, $countDaysPaidTimeOff)
    {
        $this->phpExcel = $phpExcel;
        $this->em = $em;
        $this->calendar = $calendar;
        $this->countDaysPaidTimeOff = $countDaysPaidTimeOff;
    }

    /**
     * @return array
     */
    protected function getHeaderStyle()
    {
        // array of styles for header cells
        return array(
                    'font' => array(
                        'bold' => true,
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                );
    }

    /**
     * @return array
     */
    protected function getHeaderStyleMonthly()
    {
        // array of styles for header cells
        return array(
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => '538DD5'
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    /**
     * @return array
     */
    protected function getHeaderStyleCombinedMonthly()
    {
        // array of styles for header cells
        return array(
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => 'C5D9F1'
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    /**
     * @return array
     */
    protected function getTotalStyle()
    {
        // array of styles for the cells "TOTAL"
        return array(
                    'font' => array(
                        'bold' => true,
                    ),
                    'fill' => array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'rotation' => 0,
                        'startcolor' => array(
                            'rgb' => '92D050'
                        ),
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                );
    }

    /**
     * @return array
     */
    protected function getTimeCellStyle()
    {
        // array of styles for cells with time employee
        return array(
                    'fill' => array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'rotation' => 0,
                        'startcolor' => array(
                            'rgb' => 'CCFFCC'
                        ),
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                );
    }

    protected function getNullTimeCellStyle()
    {
        // array of styles for null cells with time employee
        return array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => 'DA9694'
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    protected function getHolidayTimeCellStyle()
    {
        // array of styles for null cells with time employee
        return array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => 'FFFF99'
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    /**
     * @return array
     */
    protected function getActivityCellStyle()
    {
        // array of styles for cells with other types of employment (selfeducation, rfx...)
        return array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => 'B7DEE8'
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    protected function getColorCellStyle($color)
    {
        return array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'rotation' => 0,
                'startcolor' => array(
                    'rgb' => $color
                ),
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    }

    /**
     * @param array $employeesData
     * @return array
     */
    public function initEmployeesWeeklyData(array $employeesData)
    {
        // generate an array of managers for filling data from csv
        foreach($employeesData as $key => $empObj) {
            if($empObj->getIsManager()){
                $employeesData['managers'][$empObj->getEnName()] = array(
                    'dbProperty' => $empObj,
                    'excProperty' => array(
                        'day' => array(
                            'Mon' => array('total' => 0, 'day' => ''),
                            'Tue' => array('total' => 0, 'day' => ''),
                            'Wed' => array('total' => 0, 'day' => ''),
                            'Thu' => array('total' => 0, 'day' => ''),
                            'Fri' => array('total' => 0, 'day' => ''),
                            'Sat' => array('total' => 0, 'day' => ''),
                            'Sun' => array('total' => 0, 'day' => ''),
                        ),
                        'selfeducation' => 0,
                        'rfx' => 0,
                        'interviews' => 0,
                        'trainings' => 0,
                    )
                );
            // generate an array of employees for filling data from csv
            } else {
                $employeesData['employees'][$empObj->getEnName()] = array(
                    'dbProperty' => $empObj,
                    'excProperty' => array(
                        'day' => array(
                            'Mon' => array('total' => 0, 'day' => ''),
                            'Tue' => array('total' => 0, 'day' => ''),
                            'Wed' => array('total' => 0, 'day' => ''),
                            'Thu' => array('total' => 0, 'day' => ''),
                            'Fri' => array('total' => 0, 'day' => ''),
                            'Sat' => array('total' => 0, 'day' => ''),
                            'Sun' => array('total' => 0, 'day' => ''),
                        ),
                        'selfeducation' => 0,
                        'rfx' => 0,
                        'interviews' => 0,
                        'trainings' => 0,
                    )
                );
            }
            unset($employeesData[$key]);
        }

        ksort($employeesData['employees']);
        ksort($employeesData['managers']);

        return $employeesData;
    }

    /**
     * @param array $employeesData
     * @return array
     */
    public function initEmployeesMonthlyData(array $employeesData)
    {
        $employeesList = array();
        // generate an array of all employees for filling data from csv
        foreach($employeesData as $empObj) {
            $employeesList['employees'][$empObj->getEnName()] = array(
                'dbProperty' => $empObj,
                'excProperty' => array(
                    self::MONTHLY_ALL_TIME_INDEX => 0,
                    self::MONTHLY_TIME_BY_DATE_INDEX => array(),
                ),
            );
        }
        ksort($employeesList);

        return $employeesList;
    }

    /**
     * @param $newXlsSheet
     * @return mixed
     */
    public function getHeader(\PHPExcel_Worksheet $newXlsSheet)
    {
        // forming a header row in the output file
        $newXlsSheet->setTitle('Sheet');
        $newXlsSheet->setCellValue('A1', '')->getStyle('A1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('B1', 'Понедельник')->getStyle('B1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('C1', 'Вторник')->getStyle('C1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('D1', 'Среда')->getStyle('D1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('E1', 'Четверг')->getStyle('E1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('F1', 'Пятница')->getStyle('F1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('G1', 'Суббота')->getStyle('G1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('H1', 'Воскресенье')->getStyle('H1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('K1', 'Selfeducation')->getStyle('K1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('L1', 'RFX')->getStyle('L1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('M1', 'Interviews/Hirings')->getStyle('M1')->applyFromArray($this->getHeaderStyle());
        $newXlsSheet->setCellValue('N1', 'Trainings')->getStyle('N1')->applyFromArray($this->getHeaderStyle());

        return $newXlsSheet;
    }

    public function getHeaderMonthly(\PHPExcel_Worksheet $newXlsSheet)
    {
// forming a header row in the output file
        $newXlsSheet->setTitle('Sheet');
        $newXlsSheet->setCellValue('A1', '#')->getStyle('A1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('B1', 'Имя')->getStyle('B1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('C1', 'Координатор')->getStyle('C1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('D1', 'Ставка')->getStyle('D1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('E1', 'Отпуск')->getStyle('E1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('F1', '')->getStyle('F1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->mergeCells('E1:F1');

        $newXlsSheet->setCellValue('E2', 'Рабочий')->getStyle('E2')->applyFromArray($this->getHeaderStyleCombinedMonthly());
        $newXlsSheet->setCellValue('F2', 'Календарных')->getStyle('F2')->applyFromArray($this->getHeaderStyleCombinedMonthly());

        $newXlsSheet->setCellValue('G1', 'Больничные (есть справка)')->getStyle('G1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('H1', 'Соц. дни')->getStyle('H1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('I1', 'Тестовый срок')->getStyle('I1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('J1', 'Рабочие дни (не включает тестовый срок)')->getStyle('J1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('K1', 'Всего дней к оплате (без учета овертаймов и отпусков)')->getStyle('K1')->applyFromArray($this->getHeaderStyleMonthly());

        $newXlsSheet->setCellValue('L1', 'Овертаймы')->getStyle('L1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('M1', '')->getStyle('M1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->mergeCells('L1:M1');

        $newXlsSheet->setCellValue('L2', '1')->getStyle('L2')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('M2', '1.5')->getStyle('M2')->applyFromArray($this->getHeaderStyleMonthly());


        $newXlsSheet->setCellValue('N1', 'Отгулы')->getStyle('N1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('O1', 'Всего часов к оплате (без учета отпусков)')->getStyle('O1')->applyFromArray($this->getHeaderStyleMonthly());
        $newXlsSheet->setCellValue('P1', 'Комментарии')->getStyle('P1')->applyFromArray($this->getHeaderStyleMonthly());

        $newXlsSheet->getStyle('A1:P1')->getAlignment()->setWrapText(true);
        $newXlsSheet->getStyle('A1:P1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $newXlsSheet->getStyle('A2:P2')->applyFromArray($this->getHeaderStyleCombinedMonthly());

        $newXlsSheet->getColumnDimension('A')->setWidth(30);
        $newXlsSheet->getColumnDimension('F')->setWidth(30);
        $newXlsSheet->getColumnDimension('J')->setWidth(30);
        $newXlsSheet->getColumnDimension('K')->setWidth(30);
        $newXlsSheet->getColumnDimension('O')->setWidth(30);
        $newXlsSheet->getColumnDimension('P')->setWidth(30);

        $newXlsSheet->freezePane('A2');
        $newXlsSheet->freezePane('A3');

        return $newXlsSheet;
    }

    /**
     * @param $employeesData
     * @return \PHPExcel
     */
    public function setEmployeesWeeklyData(array $employeesData)
    {
        $newXls = $this->phpExcel->createPHPExcelObject();
        $newXls->setActiveSheetIndex(0);

        $xlsSheet = $newXls->getActiveSheet();
        $xlsSheet = $this->getHeader($xlsSheet);
        // counter for strings
        $i  = 2;

        // fill new data of Excel file
        // employees:
        $this->weeklyDays = $employeesData['weekly'];
        foreach ($employeesData['employees'] as $empName => $empData) {
            $xlsSheet->setCellValue('A' . $i, $empData['dbProperty']->getRuName() . " ({$empName})");
            $this->addDayTime($xlsSheet, $empData, $i, 'B', 'Mon');
            $this->addDayTime($xlsSheet, $empData, $i, 'C', 'Tue');
            $this->addDayTime($xlsSheet, $empData, $i, 'D', 'Wed');
            $this->addDayTime($xlsSheet, $empData, $i, 'E', 'Thu');
            $this->addDayTime($xlsSheet, $empData, $i, 'F', 'Fri');
            $this->addDayTime($xlsSheet, $empData, $i, 'G', 'Sat');
            $this->addDayTime($xlsSheet, $empData, $i, 'H', 'Sun');
            $this->addSelfeducation($xlsSheet, $empData, $i);
            $this->addRfx($xlsSheet, $empData, $i);
            $this->addInterviews($xlsSheet, $empData, $i);
            $this->addTrainings($xlsSheet, $empData, $i);
            $i++;
        }

        $i++;

        $xlsSheet = $this->setTotalRow($xlsSheet, $i);

        $i++;$i++;

        // fill new data of Excel file
        // managers:
        foreach ($employeesData['managers'] as $empName => $empData) {
            $xlsSheet->setCellValue('A' . $i, $empData['dbProperty']->getRuName() . " ({$empName})");
            $this->addDayTime($xlsSheet, $empData, $i, 'B', 'Mon');
            $this->addDayTime($xlsSheet, $empData, $i, 'C', 'Tue');
            $this->addDayTime($xlsSheet, $empData, $i, 'D', 'Wed');
            $this->addDayTime($xlsSheet, $empData, $i, 'E', 'Thu');
            $this->addDayTime($xlsSheet, $empData, $i, 'F', 'Fri');
            $this->addDayTime($xlsSheet, $empData, $i, 'G', 'Sat');
            $this->addDayTime($xlsSheet, $empData, $i, 'H', 'Sun');
            $this->addSelfeducation($xlsSheet, $empData, $i);
            $this->addRfx($xlsSheet, $empData, $i);
            $this->addInterviews($xlsSheet, $empData, $i);
            $this->addTrainings($xlsSheet, $empData, $i);
            $i++;
        }

        $xlsSheet = $this->setColumnDimension($xlsSheet);

        return $newXls;
    }

    /**
     * @param $employeesData
     * @return \PHPExcel
     */
    public function setEmployeesMonthlyData(array $employeesData)
    {
        $newXls = $this->phpExcel->createPHPExcelObject();
        $newXls->setActiveSheetIndex(0);
        $xlsSheet = $newXls->getActiveSheet();
        $xlsSheet = $this->getHeaderMonthly($xlsSheet);
        $xlsSheet = $this->prepareSheet($xlsSheet, 'Monthly Report');
        $i = 3;
        $number = 1;

        foreach ($employeesData['employees'] as $name => $data) {

            $xlsSheet->setCellValue('A'.$i, $number);
            $xlsSheet->setCellValue('B'.$i, $name);
            $xlsSheet->setCellValue('C'.$i, $this->coordinator);
            $xlsSheet->setCellValue('D'.$i, $data['dbProperty']->getRate()->getTitle());

            $holiday = $this->getWorkHolidayDays($data['dbProperty'], $employeesData['settings'], $data['excProperty']);

            if ($holiday['workHoliday']) {
                $xlsSheet->setCellValue('E'.$i, $holiday['workHoliday'])->getStyle('E' . $i)->applyFromArray($this->getColorCellStyle('FFC7CE'));
                $xlsSheet->setCellValue('F'.$i, $holiday['calendar'])->getStyle('F' . $i)->applyFromArray($this->getColorCellStyle('FFC7CE'));
            }else {
                $xlsSheet->setCellValue('E'.$i, $holiday['workHoliday']);
                $xlsSheet->setCellValue('F'.$i, $holiday['calendar']);
            }

            $xlsSheet->setCellValue('G'.$i, $holiday['hospitalDay']);
            $xlsSheet->setCellValue('H'.$i, $holiday['socDay']);

            $xlsSheet->setCellValue('I'.$i, $holiday['testDay']);
            if ($holiday['testDay'] != 0 && $holiday['workDay'] == 0) {
                $xlsSheet->getStyle('A'.$i . ':P' . $i)->applyFromArray($this->getColorCellStyle('66FFFF'));
            }

            $xlsSheet->setCellValue('J'.$i, $holiday['workDay']);
            $xlsSheet->setCellValue('K' . $i, '=SUM(G'.$i.':J' . $i . ')')->getStyle('K' . $i)->applyFromArray($this->getHeaderStyleCombinedMonthly());
            if ($holiday['overRes']['1']) {
                $xlsSheet->setCellValue('L'.$i, $holiday['overRes']['1'])->getStyle('L' . $i)->applyFromArray($this->getColorCellStyle('FFEB9C'));
            }else {
                $xlsSheet->setCellValue('L'.$i, '');
            }
            if ($holiday['overRes']['1.5']) {
                $xlsSheet->setCellValue('M'.$i, $holiday['overRes']['1.5'])->getStyle('L' . $i)->applyFromArray($this->getColorCellStyle('FFEB9C'));
            }else {
                $xlsSheet->setCellValue('M'.$i, '');
            }

            $xlsSheet->setCellValue('N'.$i, $holiday['overRes']['residue']);
            $xlsSheet->setCellValue('O' . $i, '=SUM(K'.$i.'+L' . $i . '+M' . $i . ') * 8 * D' . $i)->getStyle('O' . $i)->applyFromArray($this->getHeaderStyleCombinedMonthly());

            if ($data['dbProperty']->getFiredDate()) {
                if ($data['dbProperty']->getFiredDate()->format('m') == $employeesData['settings']['currentMonthly']) {
                    $xlsSheet->getStyle('A'.$i . ':P' . $i)->applyFromArray($this->getColorCellStyle('BFBFBF'));

                    $holiday['comment'] .= ' Уволен с ' . $data['dbProperty']->getFiredDate()->format('d.m');
                }
            }

            $xlsSheet->setCellValue('P'.$i, $holiday['comment']);

            $xlsSheet->getStyle('P' . $i)->getAlignment()->setWrapText(true);
            $xlsSheet->getStyle('P' . $i)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $i++;
            $number++;
        }

        $newXls->setActiveSheetIndex(0);

        return $newXls;
    }

    /**
     * @param Employee $employee
     * @param $settings
     * @param $data
     * @return array
     */
    protected function getWorkHolidayDays(Employee $employee, $settings, $data)
    {
        $timeOff = $this->em->getRepository('ExcCommandBundle:TimeOff')->getHolidayToMonthly($employee, $settings['currentMonthly']);

        $holiday = ['workHoliday' => 0, 'calendar' => 0, 'totalSocDay' => 0, 'testDay' => 0, 'workDay' => 0, 'hospitalDay' => 0, 'comment' => '', 'socDay' => 0];

        $calendar = $this->calendar->getCalendar();

        $dayHoliday = [];

        $holidayDayCountMonth = [];

        $calendarMonthly = $calendar[$this->calendar->getYear()][$settings['currentMonthly']];
        $calendarYear = $calendar[$this->calendar->getYear()];

        if ($timeOff) {

            foreach ($timeOff as $key => $value) {

                if (date('m', strtotime($value->getStartDate()->format('Y-m-d'))) >= $settings['currentMonthly']) {

                    $holiday['comment'] .= ' Отпуск с ' . $value->getStartDate()->format('Y-m-d') . ' по ' . $value->getEndDate()->format('Y-m-d') . '. ';

                    $dateStart = new \DateTime($value->getStartDate()->format('Y-m-d'));
                    $dateEnd = new \DateTime($value->getEndDate()->format('Y-m-d'));
                    $dateEnd->add(new \DateInterval('P1D'));

                    $period = new \DatePeriod($dateStart, new \DateInterval('P1D'), $dateEnd);

                    foreach (iterator_to_array($period) as $day) {
                        $dayHoliday[$day->format('Y-m-d')] = [
                            'date' => $day->format('Y-m-d'),
                            'typeHoliday' => $value->getTypeId(),
                        ];
                    }
                }
            }

            foreach ($dayHoliday as $key => $value) {
                foreach ($calendarYear as $keyMonthly => $monthlyDay) {

                    if ($keyMonthly == date('m', strtotime($value['date']))) {

                        $infoDay = $monthlyDay[date('d', strtotime($value['date']))];

                        if ($infoDay['date'] == $value['date']) {

                            if ($infoDay['type'] == CalendarParser::WORK_DAY) {
                                switch ($value['typeHoliday']) {
                                    /*Hospital day*/
                                    case 5:
                                        $holiday['hospitalDay'] += 1;
                                        break;
                                    /*Social Day*/
                                    case 3:
                                        $holiday['socDay'] += 1;
                                        break;
                                    default:
                                        $holiday['workHoliday'] += 1;
                                        break;
                                }

                                if ($keyMonthly == $settings['currentMonthly']) {
                                    $holidayDayCountMonth[$value['date']] = $value['date'];
                                }
                            }
                            $holiday['calendar'] += 1;
                        }
                    }
                }
            }
        }

        $dayTestPeriod = [];
        if ($employee->getStartTestPeriod()) {
            $dateStart = new \DateTime($employee->getStartTestPeriod()->format('Y-m-d'));
            $dateEnd = new \DateTime($employee->getEndTestPeriod()->format('Y-m-d'));
            $dateEnd->add(new \DateInterval('P1D'));

            $period = new \DatePeriod($dateStart, new \DateInterval('P1D'), $dateEnd);

            $dayTestPeriod = array_map(
                function($item){
                    return $item->format('Y-m-d');
                },
                iterator_to_array($period)
            );
        }

        /*Test Period And Work Period (no Holiday)*/
        foreach ($calendarMonthly as $monthlyDay) {
            if ($monthlyDay['type'] == CalendarParser::WORK_DAY) {
                if ($dayTestPeriod) {
                    $find = false;
                    foreach ($dayTestPeriod as $day) {
                        if (!isset($holidayDayCountMonth[$day])) {
                            if ($monthlyDay['date'] == $day) {
                                $holiday['testDay'] += 1;
                                $find = true;
                            }
                        }
                    }
                    if (!$find) {
                        if (!isset($holidayDayCountMonth[$day])) {
                            $holiday['workDay'] += 1;
                        }
                    }
                }else {
                    if (!isset($holidayDayCountMonth[$monthlyDay['date']])) {
                        $holiday['workDay'] += 1;
                    }
                }
            }
        }

        $total = $employee->getRate()->getWorkingHours() * $holiday['workDay'];

        $currentTotal = $data[self::MONTHLY_ALL_TIME_INDEX];

        $resTime = ['1' => 0, '1.5' => 0, 'residue' => 0];

        if ($total > $currentTotal) {
            $resTime['residue'] = round(($total - $currentTotal) / $employee->getRate()->getWorkingHours(), 2);
        }elseif($total != $currentTotal) {
            $weekTotal = [];
            foreach ($data['week'] as $week => $days) {

                foreach ($days as $day => $time) {

                    if (isset($calendarMonthly[date('d', strtotime($day))])) {
                        $info = $calendarMonthly[date('d', strtotime($day))];

                        if ($info['type'] == CalendarParser::WORK_DAY) {

                            if (!isset($weekTotal[$week]['totalWork'])) {
                                $weekTotal[$week]['totalWork'] = 0;
                            }
                            $weekTotal[$week]['totalWork'] += $time['time'];

                            if (!isset($weekTotal[$week]['count'])) {
                                $weekTotal[$week]['count'] = 0;
                            }
                            $weekTotal[$week]['count'] += 1;
                        }else {
                            if (!isset($weekTotal[$week]['totalHoliday'])) {
                                $weekTotal[$week]['totalHoliday'] = 0;
                            }
                            $weekTotal[$week]['totalHoliday'] += $time['time'];
                        }
                    }
                }
            }

            foreach ($weekTotal as $week => &$valueTotal) {
                if (isset($valueTotal['count'])) {
                    $totalWeek = $valueTotal['count'] * $employee->getRate()->getWorkingHours();
                    if ($totalWeek > $valueTotal['totalWork']) {
                        $w = $totalWeek - $valueTotal['totalWork'];

                        if ($w < $valueTotal['totalHoliday'] || $valueTotal['totalHoliday'] == 0) {
                            $valueTotal['totalWork'] += $valueTotal['totalHoliday'];
                            $resTime['residue'] += $totalWeek - $valueTotal['totalWork'];
                        }else {
                            $valueTotal['totalWork'] += $w;
                            $resTime['1.5'] += $valueTotal['totalHoliday'] - $w;
                        }
                    }else {
                        $resTime['1'] += ($valueTotal['totalWork'] - $totalWeek);
                    }
                }else {
                    $resTime['1.5'] += $valueTotal['totalHoliday'];
                }
            }
        }

        foreach ($resTime as $key => $resValue) {
            if (in_array($key, array('1', '1.5'))) {
                if ($resValue != 0) {
                    $resTime[$key] = round($resTime[$key] / $employee->getRate()->getWorkingHours(), 2);
                    $holiday['comment'] .= 'Овертайм по ставке ' . $key;
                }
            }
        }

        $holiday['overRes'] = $resTime;

        $holiday['totalSocDay'] = $this->getCountPaidTimeYear($employee->getId());

        if ($holiday['socDay']) {
            $cur = $this->countDaysPaidTimeOff - $holiday['totalSocDay'];
            $holiday['comment'] .= 'Осталось ' . $cur . ' соцдня.';
        }

        return $holiday;

    }

    /**
     * @param $employeeId
     * @return mixed
     */
    protected function getCountPaidTimeYear($employeeId)
    {
        $dateStartSocDay = new \DateTime();
        $dateStartSocDay->setDate(date('Y'), 1, 1);

        $dateEndSocDay = new \DateTime();
        $dateEndSocDay->setDate(date('Y', strtotime('1 year')), 1, 1);

        return $this->em->getRepository('ExcCommandBundle:TimeOff')->getCountUserPaidTimeOffYear($dateStartSocDay, $dateEndSocDay, $employeeId);
    }

    /**
     * @param $newXls
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function getExcelResponse(\PHPExcel $newXls, $type)
    {
        // form the answer for returns file
        $fileName = $type == User::WEEKLY_REPORT ? date("d-m-y") : date('F.Y');
        $writer = $this->phpExcel->createWriter($newXls, 'Excel2007');
        $response = $this->phpExcel->createStreamedResponse($writer);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . $fileName . '.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param $i
     * @return \PHPExcel_Worksheet
     */
    protected function setTotalRow(\PHPExcel_Worksheet $xlsSheet, $i)
    {
        // write the final hours
        $sumLimit = $i - 2;
        $xlsSheet->setCellValue('A' . $i, 'ИТОГО')->getStyle('A' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('B' . $i, '=SUM(B2:B' . $sumLimit . ')')->getStyle('B' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('C' . $i, '=SUM(C2:C' . $sumLimit . ')')->getStyle('C' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('D' . $i, '=SUM(D2:D' . $sumLimit . ')')->getStyle('D' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('E' . $i, '=SUM(E2:E' . $sumLimit . ')')->getStyle('E' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('F' . $i, '=SUM(F2:F' . $sumLimit . ')')->getStyle('F' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('G' . $i, '=SUM(G2:G' . $sumLimit . ')')->getStyle('G' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('H' . $i, '=SUM(H2:H' . $sumLimit . ')')->getStyle('H' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('K' . $i, '=SUM(K2:K' . $sumLimit . ')')->getStyle('K' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('L' . $i, '=SUM(L2:L' . $sumLimit . ')')->getStyle('L' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('M' . $i, '=SUM(M2:M' . $sumLimit . ')')->getStyle('M' . $i)->applyFromArray($this->getTotalStyle());
        $xlsSheet->setCellValue('N' . $i, '=SUM(N2:N' . $sumLimit . ')')->getStyle('N' . $i)->applyFromArray($this->getTotalStyle());

        return $xlsSheet;
    }

    /**
     * @param $xlsSheet
     * @return mixed
     */
    protected function setColumnDimension(\PHPExcel_Worksheet $xlsSheet)
    {
        // set the column width for returns files
        $xlsSheet->getColumnDimension('A')->setWidth(20);
        $xlsSheet->getColumnDimension('B')->setWidth(15);
        $xlsSheet->getColumnDimension('C')->setWidth(15);
        $xlsSheet->getColumnDimension('D')->setWidth(15);
        $xlsSheet->getColumnDimension('E')->setWidth(15);
        $xlsSheet->getColumnDimension('F')->setWidth(15);
        $xlsSheet->getColumnDimension('G')->setWidth(15);
        $xlsSheet->getColumnDimension('H')->setWidth(15);
        $xlsSheet->getColumnDimension('K')->setWidth(15);
        $xlsSheet->getColumnDimension('L')->setWidth(15);
        $xlsSheet->getColumnDimension('M')->setWidth(18);
        $xlsSheet->getColumnDimension('N')->setWidth(15);

        return $xlsSheet;
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param array $empData
     * @param $i
     * @param $cellIndex
     * @param $day
     */
    protected function addDayTime(\PHPExcel_Worksheet &$xlsSheet, array $empData, $i, $cellIndex, $day)
    {
        $timeOff = $this->em->getRepository('ExcCommandBundle:TimeOff')->getHolidayToDay( $empData['excProperty']['day'][$day]['day'], $empData['dbProperty']);

        $calendar = $this->calendar->getHolidayCalendar();

        if ($timeOff) {

            $typeTimeOff = $this->em->getRepository('ExcCommandBundle:TypeTimeOff')->findOneBy(array('id' => $timeOff->getType()));

            if (!empty($empData['excProperty']['day'][$day]['total'])) {
                $xlsSheet->setCellValue($cellIndex . $i, $empData['excProperty']['day'][$day]['total'])->getStyle($cellIndex. $i)->applyFromArray($this->getHolidayTimeCellStyle());

                /**
                 * Add comment Excel
                 */
                $xlsSheet->getComment($cellIndex . $i)->getText()->createTextRun($typeTimeOff->getTitle());
            }else {
                $xlsSheet->setCellValue($cellIndex . $i, $typeTimeOff->getTitle())->getStyle($cellIndex. $i)->applyFromArray($this->getNullTimeCellStyle());

                /**
                 * Add comment Excel
                 */
                $xlsSheet->getComment($cellIndex . $i)->getText()->createTextRun('c ' . $timeOff->getStartDate()->format('Y-m-d') . ' по ' . $timeOff->getEndDate()->format('Y-m-d'));
            }
        }else {

            //set DayTime cell value, add cell style
            if (0 !== $empData['excProperty']['day'][$day]['total']) {
                if (isset($calendar[$this->weeklyDays[$day]])) {
                    $xlsSheet->setCellValue($cellIndex . $i, $empData['excProperty']['day'][$day]['total'])->getStyle($cellIndex. $i)->applyFromArray($this->getTimeCellStyle());
                }else {
                    $xlsSheet->setCellValue($cellIndex . $i, $empData['excProperty']['day'][$day]['total'])->getStyle($cellIndex. $i)->applyFromArray($this->getTimeCellStyle());
                }
            } else {
                if (isset($calendar[$this->weeklyDays[$day]])) {
                    $xlsSheet->setCellValue($cellIndex . $i, '');
                }else {
                    $xlsSheet->setCellValue($cellIndex . $i, $empData['excProperty']['day'][$day]['total'])->getStyle($cellIndex. $i)->applyFromArray($this->getNullTimeCellStyle());
                }
            }
        }
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param array $empData
     * @param $i
     */
    protected function addSelfeducation(\PHPExcel_Worksheet &$xlsSheet, array $empData, $i)
    {
        //set selfeducation cell value, add cell style
        if (0 !== $empData['excProperty']['selfeducation']) {
            $xlsSheet->setCellValue('K' . $i, $empData['excProperty']['selfeducation'])->getStyle('K'. $i)->applyFromArray($this->getActivityCellStyle());
        } else {
            $xlsSheet->setCellValue('K' . $i, '')->getStyle('K'. $i)->applyFromArray($this->getActivityCellStyle());
        }
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param array $empData
     * @param $i
     */
    protected function addRfx(\PHPExcel_Worksheet &$xlsSheet, array $empData, $i)
    {
        //set rfx cell value, add cell style
        if (0 !== $empData['excProperty']['rfx']) {
            $xlsSheet->setCellValue('L' . $i, $empData['excProperty']['rfx'])->getStyle('L'. $i)->applyFromArray($this->getActivityCellStyle());
        } else {
            $xlsSheet->setCellValue('L' . $i, '')->getStyle('L'. $i)->applyFromArray($this->getActivityCellStyle());
        }
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param array $empData
     * @param $i
     */
    protected function addInterviews(\PHPExcel_Worksheet &$xlsSheet, array $empData, $i)
    {
        //set interviews cell value, add cell style
        if (0 !== $empData['excProperty']['interviews']) {
            $xlsSheet->setCellValue('M' . $i, $empData['excProperty']['interviews'])->getStyle('M'. $i)->applyFromArray($this->getActivityCellStyle());
        } else {
            $xlsSheet->setCellValue('M' . $i, '')->getStyle('M'. $i)->applyFromArray($this->getActivityCellStyle());
        }
    }

    /**
     * @param \PHPExcel_Worksheet $xlsSheet
     * @param array $empData
     * @param $i
     */
    protected function addTrainings(\PHPExcel_Worksheet &$xlsSheet, array $empData, $i)
    {
        //set trainings cell value, add cell style
        if (0 !== $empData['excProperty']['trainings']) {
            $xlsSheet->setCellValue('N'.$i, $empData['excProperty']['trainings'])->getStyle('N'.$i)->applyFromArray(
                $this->getActivityCellStyle()
            );
        } else {
            $xlsSheet->setCellValue('N'.$i, '')->getStyle('N'.$i)->applyFromArray($this->getActivityCellStyle());
        }
    }

    private function prepareSheet(\PHPExcel_Worksheet $newXlsSheet, $title)
    {
        $newXlsSheet->setTitle($title);

        return $newXlsSheet;
    }
}