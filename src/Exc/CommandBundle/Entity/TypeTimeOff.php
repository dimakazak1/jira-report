<?php

namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeTimeOff
 */
class TypeTimeOff
{
    const UNPAID_LEAVE = 'unpaid_leave';
    const TRANSFER = 'transfer';
    const PAID_TIME_OFF = 'paid_time_off';
    const VACATION = 'vacation';
    const SICK_LEAVE = 'sick_leave';
    const RECRUITMENT_OFFICE = 'recruitment_office';
    const FORMAL_HOLIDAY = 'formal_holiday';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $defaultText;

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getTitle());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return TypeTimeOff
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TypeTimeOff
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set default text
     *
     * @param string $defaultText
     * @return TypeTimeOff
     */
    public function setDefaultTex($defaultText)
    {
        $this->defaultText = $defaultText;

        return $this;
    }

    /**
     * Get efault text
     *
     * @return string
     */
    public function getDefaultText()
    {
        return $this->defaultText;
    }
}
