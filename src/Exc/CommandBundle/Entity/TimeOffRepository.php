<?php
namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

class TimeOffRepository extends EntityRepository
{
    /**
     * @param $date
     * @param $timeOff
     * @return mixed
     */
    public function getCountUserPaidTimeOff($date, $timeOff)
    {
        return $this
            ->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->where('t.createdAt >= :lastYear AND t.employeeId = :employeeId AND t.typeId = 3')
            ->setParameters(array(
                    'lastYear' => $date,
                    'employeeId' => $timeOff->getEmployee()->getId(),
                )
            )
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $date
     * @param $employee
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getHolidayToDay($date, $employee)
    {
        $query = $this
            ->createQueryBuilder('t')
            ->where(':date BETWEEN t.startDate AND t.endDate AND t.employeeId = :employeeId AND t.status = :status')
            ->setParameters(array(
                    'date' => $date,
                    'employeeId' => $employee->getId(),
                    'status' => TimeOff::STATUS_APPROVED
                )
            )
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            return $query->getSingleResult();
        }catch(\Doctrine\ORM\NoResultException $e) {

            return null;
        }
    }

    /**
     * @param $userId
     * @return array|null
     */
    public function getTimeOffList($userId) {

        $list = $this->createQueryBuilder('t')
            ->where('t.employeeId = :employeeId')
            ->setParameter('employeeId', $userId)
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        if (!empty($list)) {
            return $list;
        }

        return null;
    }

    /**
     * @param $userId
     * @return \Doctrine\ORM\Query
     */
    public function getTimeOffListQuery($userId)
    {
        return $this->createQueryBuilder('t')
            ->where('t.employeeId = :employeeId')
            ->setParameter('employeeId', $userId)
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery();
    }

    /**
     * @param Employee $employee
     * @param $monthly
     * @return array
     */
    public function getHolidayToMonthly(Employee $employee, $monthly)
    {
        return $this
            ->createQueryBuilder('t')
            ->where(':monthly BETWEEN MONTH(t.startDate) AND MONTH(t.endDate) AND t.employeeId = :employeeId AND t.status = :status')
            ->setParameters(array(
                    'monthly' => $monthly,
                    'employeeId' => $employee->getId(),
                    'status' => TimeOff::STATUS_APPROVED
                )
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @param $employeeId
     * @return mixed
     */
    public function getCountUserPaidTimeOffYear($dateStart, $dateEnd, $employeeId)
    {
        return $this
            ->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->where('t.createdAt BETWEEN :startDate AND :endDate AND t.employeeId = :employeeId AND t.typeId = 3')
            ->setParameters(array(
                    'startDate' => $dateStart,
                    'endDate' => $dateEnd,
                    'employeeId' => $employeeId,
                )
            )
            ->getQuery()
            ->getSingleScalarResult();
    }
}