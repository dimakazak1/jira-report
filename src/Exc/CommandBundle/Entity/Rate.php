<?php

namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 */
class Rate
{
    /**
     * @var integer
     */
    private $id;


    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $workingHours;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return integer
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TypeTimeOff
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get workingHours
     *
     * @return integer
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * Set title
     *
     * @param integer $workingHours
     * @return TypeTimeOff
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = $workingHours;

        return $this;
    }
}
