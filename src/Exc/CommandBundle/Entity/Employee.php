<?php

namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Exc\CommandBundle\Entity\Rate;

/**
 * Class Employee
 * @package Exc\CommandBundle\Entity
 */
class Employee implements UserInterface, \Serializable
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $timeOff;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $rate;

    /**
     * @var integer
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $username;

    /**
     * @var string
     *
     */
    private $enName;

    /**
     * @var string
     *
     */
    private $ruName;

    /**
     * @var boolean
     *
     */
    private $isManager;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime $passwordChangedAt
     */
    private $passwordChangedAt;

    /**
     * @var string
     */
    private $passwordResetToken;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var integer
     */
    private $rateId;

    /**
     * @var datetime $startTestPeriod
     */
    private $startTestPeriod;

    /**
     * @var datetime $endTestPeriod
     */
    private $endTestPeriod;

    /**
     * @var datetime $firedDate
     */
    private $firedDate;

    public function __construct()
    {
        $this->salt = md5(uniqid());
        $this->username = $this->enName;
        $this->timeOff = new ArrayCollection();
        $this->role = 'ROLE_USER';
    }

    /**
     * @return mixed
     */
    public function getStartTestPeriod()
    {
        return $this->startTestPeriod;
    }

    /**
     * @param mixed $startTestPeriod
     * @return datetime
     */
    public function setStartTestPeriod($startTestPeriod)
    {
        $this->startTestPeriod = $startTestPeriod;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndTestPeriod()
    {
        return $this->endTestPeriod;
    }

    /**
     * @param mixed $endTestPeriod
     * @return datetime
     */
    public function setEndTestPeriod($endTestPeriod)
    {
        $this->endTestPeriod = $endTestPeriod;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getEnName()) ? : '';
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($pass)
    {
        $this->plainPassword = $pass;

        return $this;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set enName
     *
     * @param string $enName
     * @return Employee
     */
    public function setEnName($enName)
    {
        $this->enName = $enName;

        return $this;
    }

    /**
     * Get enName
     *
     * @return string
     */
    public function getEnName()
    {
        return $this->enName;
    }

    /**
     * Set ruName
     *
     * @param string $ruName
     * @return Employee
     */
    public function setRuName($ruName)
    {
        $this->ruName = $ruName;

        return $this;
    }

    /**
     * Get ruName
     *
     * @return string
     */
    public function getRuName()
    {
        return $this->ruName;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set isManager
     *
     * @param boolean $isManager
     * @return Employee
     */
    public function setIsManager($isManager)
    {
        $this->isManager = $isManager;

        return $this;
    }

    /**
     * Get isManager
     *
     * @return boolean
     */
    public function getIsManager()
    {
        return $this->isManager;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $passwordResetToken
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
    }

    /**
     * @return string
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * @param $passwordChangedAt
     */
    public function setPasswordChangedAt($passwordChangedAt)
    {
        $this->passwordChangedAt = $passwordChangedAt;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordChangedAt()
    {
        return $this->passwordChangedAt;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set role
     * @param $role
     *  @return User
     */
    public function setRoles($role)
    {
        $this->role = $role;
    }
    /**
     * Get role
     *  @return string
     */
    public function getRoles()
    {
        return array(
            $this->role,
        );
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {

        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Employee
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $timeOff
     */
    public function setTimeOff($timeOff)
    {
        $this->timeOff = $timeOff;
    }

    /**
     * @return mixed
     */
    public function getTimeOff()
    {
        return $this->timeOff;
    }

    public function getRateId()
    {
        return $this->rateId;
    }

    /**
     * Set rate
     *
     * @param integer $rateId
     * @return Employee
     */
    public function setRateId($rateId)
    {
        $this->rateId = $rateId;

        return $this;
    }

    /**
     * @param mixed $rate
     */
    public function setRate(Rate $rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return datetime
     */
    public function getFiredDate()
    {
        return $this->firedDate;
    }

    /**
     * @param datetime $firedDate
     * @return Employee
     */
    public function setFiredDate($firedDate)
    {
        $this->firedDate = $firedDate;

        return $this;
    }

}
