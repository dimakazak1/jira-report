<?php
namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Entity\TypeTimeOff;

/**
 * TimeOff
 */
class TimeOff
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_NOT_APPROVED = 3;
    const DEFAULT_TIME_OFF_STATUS = self::STATUS_PENDING;
    const DEFAULT_LIMIT = 10;
    /**
     * @var
     */
    private $employee;

    /**
     * @var
     */
    private $manager;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var TypeTimeOff
     */
    private $type;

    /**
     * @var integer $typeId
     */
    private $typeId;

    /**
     * @var string
     */
    private $reason;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $employeeId;

    /**
     * @var integer
     */
    private $managerId;

    public function __construct()
    {
        $this->status = self::DEFAULT_TIME_OFF_STATUS;
    }

    public function __toString()
    {
        return 'Time off';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param TypeTimeOff $type
     */
    public function setType(TypeTimeOff $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return TypeTimeOff
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return TimeOff
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TimeOff
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return TimeOff
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return TimeOff
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set employeeId
     *
     * @param integer $employeeId
     * @return TimeOff
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;

        return $this;
    }

    /**
     * Get employeeId
     *
     * @return integer
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param $managerId
     * @return $this
     */
    public function setManagerId($managerId)
    {
        $this->managerId = $managerId;

        return $this;
    }

    /**
     * Get managerId
     *
     * @return int
     */
    public function getManagerId()
    {
        return $this->managerId;
    }

    /**
     * @param mixed $manager
     */
    public function setManager(Employee $manager = null)
    {
        $this->manager = $manager;
    }

    /**
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param $typeId
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }
}
