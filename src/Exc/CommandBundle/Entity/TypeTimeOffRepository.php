<?php
namespace Exc\CommandBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TypeTimeOffRepository extends EntityRepository
{

    /**
     * @return array
     */
    public function getTypeTimeOffArray()
    {
        return $this
            ->createQueryBuilder('e')
            ->select('e')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

}