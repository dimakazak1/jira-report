<?php
namespace Exc\CommandBundle\Form\Type\TimeOff;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class AddTimeOffTypeManager extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', 'choice', array(
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control select2-select',
                    ),
                    'choices' => array(
                        TimeOff::STATUS_PENDING => 'Pending',
                        TimeOff::STATUS_APPROVED => 'Approved',
                        TimeOff::STATUS_NOT_APPROVED => 'Declined',
                    ),
                )
            )
            ->add('type', 'entity', array(
                    'required' => false,
                    'class' => 'ExcCommandBundle:TypeTimeOff',
                    'attr' => array(
                        'class' => 'form-control select2-select',
                    ),
                    'property' => 'title',
                )
            )
            ->add('startDate', 'date', array(
                    'required' => false,
                    'label' => 'Start Date',
                    'input'  => 'datetime',
                    'widget' => 'single_text',

                    'data' => new \DateTime(),
                )
            )
            ->add('endDate', 'date', array(
                    'required' => false,
                    'label' => 'End Date',
                    'input'  => 'datetime',
                    'widget' => 'single_text',
                    'data' => new \DateTime('tomorrow '),
                )
            )
            ->add('reason', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => ' form-control',
                ),
            ))
            ->add('Send', 'submit', array(
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
            ->getForm();
    }

    /**
     * @param array $options
     * @return array
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Exc\CommandBundle\Entity\TimeOff',
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'addTimeOff';
    }

}