<?php
namespace Exc\CommandBundle\Parser;

use Doctrine\Common\Cache\CacheProvider;

class CalendarParser
{
    const HOLIDAY_DAY = 'holiday';
    const WORK_DAY = 'working';

    /**
     * Время жизни кэша
     */
    const LIFE_TIME_CACHE = 2592000;

    /**
     * @var string $url
     */
    protected $url = 'http://www.calendar.by/';

    /**
     * Основная регулярка, парсет весь календарь
     * @var string $regex
     */
    protected $regex = "/<td onclick=\"document\\.location='day\\.php\\?date=(.+?)'; return false\".+?>/i";

    /**
     * Удаляем даты предыдущего и следующего месяца
     * @var string $regexDoubleText
     */
    protected $regexDoubleText = "/<td onclick=\"document\\.location='day\\.php\\?date=.{10}'; return false\" \">/";

    /**
     * Поиск даты в строке
     * @var string $regexFindDate
     */
    protected $regexFindDate = "/(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}/";

    /**
     * Поиск стиля, по этому атрибуту определяем тип дня (рабочий или выходной)
     * @var string $regexFindStyle
     */
    protected $regexFindStyle = "/bgcolor=\"#FFE0E6\"/";

    /**
     * Поиск дат которые приходяться на другие года
     * @var string $regexFindNoStyle
     */
    protected $regexFindNoStyle = "/<div class=\"content\"><font size=\\+1 color=\"#D11919\">(.+?)<\\/font>/";

    /**
     * @var CacheProvider $cache
     */
    protected $cache;

    /**
     * @var array $holiday
     */
    protected $holiday = [];

    /**
     * @var integer $year
     */
    private $year;

    /**
     * @param CacheProvider $cache
     */
    public function __construct(CacheProvider $cache) {
        $this->cache = $cache;
        $this->year = date('Y');
    }

    /**
     * @return array
     */
    protected function getListDateForYear()
    {
        $dateStart = new \DateTime(date($this->getYear() . '-01-01'));
        $dateEnd = new \DateTime(date($this->getYear() . '-01-01'));
        $dateEnd->add(new \DateInterval('P1Y'));

        $period = new \DatePeriod($dateStart, new \DateInterval('P1D'), $dateEnd);

        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );

        return $arrayOfDates;
    }

    /**
     * @return string
     */
    protected function getContent()
    {
        return file_get_contents($this->url . '?year=' . $this->getYear());
    }

    /**
     * @param $content
     * @return string
     */
    protected function removeFullTrim($content)
    {
        return trim(preg_replace('/\s{2,}/', ' ', $content));
    }

    /**
     * @param string $holiday
     * @return string
     */
    protected function generationKey($holiday = '') {
        return $this->getYear() . '-' . 'calendar' . $holiday;
    }

    /**
     * @return array
     */
    protected function parseCalendar()
    {
        $calendar = [];

        $content = $this->getContent();

        $content = $this->removeFullTrim($content);

        preg_match_all($this->regexDoubleText, $content, $doubleText);

        $content = preg_replace($this->regexDoubleText, '', $content);

        preg_match_all($this->regex, $content, $matches);

        if ($matches) {

            foreach ($matches[0] as $value) {

                preg_match($this->regexFindDate, $value, $date);
                preg_match($this->regexFindStyle, $value, $type);

                if (isset($date[0])) {
                    $day = date("Y-m-d", strtotime($date[0]));

                    $calendar[$day] = ['content' => $value, 'type' => self::WORK_DAY ];
                    if ($type) {
                        $calendar[$day]['type'] = self::HOLIDAY_DAY;

                        $this->holiday[$day] = self::HOLIDAY_DAY;
                    }
                }
            }
        }

        return $calendar;
    }

    /**
     * @return array
     */
    protected function getArrayCalendar()
    {
        $calendar = [];

        $holiday = $this->parseCalendar();

        foreach ($this->getListDateForYear() as $day) {
            $calendar[date('Y', strtotime($day))][date('m', strtotime($day))][date('d', strtotime($day))] = [
                'date' => $day,
                'day' => date('d', strtotime($day)),
                'type' => isset($holiday[$day]['type']) ? $holiday[$day]['type'] : 'work',
            ];
        }

        return $calendar;
    }

    /**
     * @return array
     */
    protected function getArrayHolidayCalendar()
    {
        $this->parseCalendar();

        return $this->holiday;
    }

    /**
     * @return mixed
     */
    public function getCalendar()
    {
        if ($calendar = $this->cache->fetch($this->generationKey())) {
            return $calendar;
        }

        $calendar = $this->getArrayCalendar();

        $this->cache->save($this->generationKey(), $calendar, self::LIFE_TIME_CACHE);

        return $calendar;
    }

    /**
     * @return mixed
     */
    public function getHolidayCalendar() {

        if ($calendar = $this->cache->fetch($this->generationKey('holiday'))) {
            return $calendar;
        }

        $calendar = $this->getArrayHolidayCalendar();

        $this->cache->save($this->generationKey('holiday'), $calendar, self::LIFE_TIME_CACHE);

        return $calendar;
    }

    /**
     * @param string $type
     */
    public function updateCalendar($type)
    {
        $calendar = $this->getArrayCalendar();

        $this->cache->save($this->generationKey($type), $calendar, self::LIFE_TIME_CACHE);
    }

    /**
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return integer $year
     */
    public function getYear()
    {
        return $this->year;
    }

} 