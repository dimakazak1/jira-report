<?php
namespace Exc\CommandBundle\Parser;

use Exc\CommandBundle\Builder\ExcelBuilder;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CSVParser
{
    const CELL_NAME = 5;
    const CELL_DATE = 6;
    const CELL_TIME = 8;

    protected $weeklyDays = ['Mon' => '', 'Tue' => '', 'Wed' => '', 'Thu' => '', 'Fri' => '', 'Sat' => '', 'Sun' => ''];

    /**
     * @param $inputFile
     * @param array $employeesData
     * @return array
     */
    public function parseWeeklyCsv($inputFile, array $employeesData)
    {
        $csvFile = fopen($inputFile, 'r');

        if (false === $csvFile) {
            return array();
        }

        // read the csv and fill array for recording of Excel
        while (($data = fgetcsv($csvFile, 1000, ';')) !== false) {

            //if employee with the name $data[self::CELL_NAME] contained in the database
            if (isset($employeesData['employees'][$data[self::CELL_NAME]])) {
                $this->addDayRow($employeesData, $data, 'employees');
                $this->addSelfRow($employeesData, $data, 'employees');
                $this->addCrmRow($employeesData, $data, 'employees');
                $this->addWdInterviewRow($employeesData, $data, 'employees');
                $this->addWdTrainingRow($employeesData, $data, 'employees');
            }
            //if employee with the name $data[self::CELL_NAME] contained in the database
            elseif (isset($employeesData['managers'][$data[self::CELL_NAME]])) {
                $this->addDayRow($employeesData, $data, 'managers');
                $this->addSelfRow($employeesData, $data, 'managers');
                $this->addCrmRow($employeesData, $data, 'managers');
                $this->addWdInterviewRow($employeesData, $data, 'managers');
                $this->addWdTrainingRow($employeesData, $data, 'managers');
            }
        }

        $employeesData['weekly'] = $this->getWeeklyDays();

        fclose($csvFile);

        return $employeesData;
    }

    /**
     * @param $inputFile
     * @param array $employeesData
     * @return array
     */
    public function parseMonthlyCsv($inputFile, array $employeesData)
    {
        $csvFile = fopen($inputFile, 'r');

        if (false === $csvFile) {
            return array();
        }

        $monthly = 0;
        // read the csv and fill array for recording of Excel
        while (($data = fgetcsv($csvFile, 1000, ';')) !== false) {
            if (isset($employeesData['employees'][$data[self::CELL_NAME]])) {
                if (!$monthly) {
                    $monthly = date('m', strtotime($data[self::CELL_DATE]));
                }
                $this->addMonthlyTimeRow($employeesData, $data);
            }
        }

        $employeesData['settings'] = [
            'currentMonthly' => $monthly,
            'weeklyMonthly' => $this->getListDateForMonthly($monthly)
        ];

        $employeesData = $this->getTimeMonthly($employeesData);

        fclose($csvFile);

        return $employeesData;
    }

    protected function getTimeMonthly($employeesData)
    {
        foreach ($employeesData['employees'] as $key => &$value) {
            foreach ($employeesData['settings']['weeklyMonthly'] as $week => $days) {
                foreach ($days as $day) {
                    if (isset($value['excProperty'][ExcelBuilder::MONTHLY_TIME_BY_DATE_INDEX][date('d.m.Y', strtotime($day))])) {
                        $value['excProperty']['week'][$week][$day]['time'] = $value['excProperty']['timeByDate'][date('d.m.Y', strtotime($day))];
                    }else {
                        $value['excProperty']['week'][$week][$day]['time'] = 0;
                    }
                }
            }
        }

        return $employeesData;
    }

    /**
     * @param $monthly
     * @return array
     */
    protected function getListDateForMonthly($monthly)
    {
        $dateStartMonthly = date('Y-' . $monthly . '-01');
        $dateEndMonthly = date('Y-m-d', strtotime(date('Y-' . $monthly . '-' . date('t', strtotime($dateStartMonthly)))));

        $dateStart = new \DateTime($dateStartMonthly);
        $dateEnd = new \DateTime($dateEndMonthly);
        $dateEnd->add(new \DateInterval('P1D'));

        $period = new \DatePeriod($dateStart, new \DateInterval('P1D'), $dateEnd);

        $arrayOfDates = array_map(
            function($item){
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );

        $monthlyWeekly = [];
        $weekly = 1;
        foreach ($arrayOfDates as $key => $value) {
            $dayWeekly = date('N', strtotime($value));
            if (!($dayWeekly % 7)) {
                $monthlyWeekly[$weekly][$value] = $value;
                $weekly++;
            }else {
                $monthlyWeekly[$weekly][$value] = $value;
            }
        }

        return $monthlyWeekly;
    }

    /**
     * @param $date
     * @return string
     */
    protected function getDayOfWeek($date)
    {
        return strftime("%a", strtotime($date));
    }

    /**
     * @param array $employeesData
     * @param array $data
     * @param $key
     */
    protected function addSelfRow(array &$employeesData, array $data, $key)
    {
        if (strpos($data[2], 'SELF') === 0) {
            $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['selfeducation'] += str_replace(',', '.', $data[self::CELL_TIME]);
        }
    }

    /**
     * @param array $employeesData
     * @param array $data
     * @param $key
     */
    protected function addCrmRow(array &$employeesData, array $data, $key)
    {
        if (strpos($data[2], 'CRM') === 0) {
            $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['rfx'] += str_replace(',', '.', $data[self::CELL_TIME]);
        }
    }

    /**
     * @param array $employeesData
     * @param array $data
     * @param $key
     */
    protected function addWdInterviewRow(array &$employeesData, array $data, $key)
    {
        if ((strpos($data[2], 'WDINTERVIEW') === 0) || (strpos($data[2], 'WDHIRING') === 0)) {
            $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['interviews'] += str_replace(',', '.', $data[self::CELL_TIME]);
        }
    }

    /**
     * @param array $employeesData
     * @param array $data
     * @param $key
     */
    protected function addWdTrainingRow(array &$employeesData, array $data, $key)
    {
        if (strpos($data[2], 'WDTRAINING') === 0) {
            $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['trainings'] += str_replace(',', '.', $data[self::CELL_TIME]);
        }
    }

    /**
     * @param array $employeesData
     * @param array $data
     * @param $key
     */
    protected function addDayRow(array &$employeesData, array $data, $key)
    {
        $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['day'][$this->getDayOfWeek($data[self::CELL_DATE])]['total'] += str_replace(',', '.', $data[self::CELL_TIME]);
        $employeesData[$key][$data[self::CELL_NAME]]['excProperty']['day'][$this->getDayOfWeek($data[self::CELL_DATE])]['day'] = date("Y-m-d", strtotime($data[self::CELL_DATE]));

        $this->weeklyDays[$this->getDayOfWeek($data[self::CELL_DATE])] = date("Y-m-d", strtotime($data[self::CELL_DATE]));
    }

    /**
     * @param array $employeesData
     * @param array $data
     */
    protected function addMonthlyTimeRow(array &$employeesData, array $data)
    {
        $time = str_replace(',', '.', $data[self::CELL_TIME]);
        if (!isset($employeesData['employees'][$data[self::CELL_NAME]]['excProperty'][ExcelBuilder::MONTHLY_TIME_BY_DATE_INDEX][$data[self::CELL_DATE]])) {
            $employeesData['employees'][$data[self::CELL_NAME]]['excProperty'][ExcelBuilder::MONTHLY_TIME_BY_DATE_INDEX][$data[self::CELL_DATE]] = 0;
        }
        $employeesData['employees'][$data[self::CELL_NAME]]['excProperty'][ExcelBuilder::MONTHLY_TIME_BY_DATE_INDEX][$data[self::CELL_DATE]] += $time;
        $employeesData['employees'][$data[self::CELL_NAME]]['excProperty'][ExcelBuilder::MONTHLY_ALL_TIME_INDEX] += $time;
    }

    /**
     * @return array
     */
    protected function getWeeklyDays()
    {
        $weeklyDays = ['Mon' => 1, 'Tue' => 2, 'Wed' => 3, 'Thu' => 4, 'Fri' => 5, 'Sat' => 6, 'Sun' => 7];

        $noEmptyDay = '';

        foreach ($this->weeklyDays as $key => $value) {
            if (!empty($value)) {
                $noEmptyDay = $value;
                break;
            }
        }

        $weeklyDay = date("w", strtotime($noEmptyDay));

        foreach ($this->weeklyDays as $key => $value) {
            if (empty($value)) {
                $date = new \DateTime($noEmptyDay);

                $difference = $weeklyDay - $weeklyDays[$key];
                if ($difference > 0) {
                    $date->modify("-" . $difference . " day");
                }else {
                    $date->modify("+" . abs($difference) . " day");
                }

                $this->weeklyDays[$key] = $date->format('Y-m-d');
            }
        }

        return $this->weeklyDays;
    }
} 