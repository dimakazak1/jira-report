<?php
namespace Exc\ExcelBundle\Controller;

use Exc\CommandBundle\Entity\Employee;
use Sonata\AdminBundle\Controller\CoreController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RecoveryPasswordController extends CoreController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(Request $request)
    {
        $form = $this->getResetPassForm();


        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $email = $data['email'];
                $count = $this->getDoctrine()->getRepository('ExcCommandBundle:Employee')->checkEmployee($email);
                if ($count) {
                    $this->setPassChangedtAt($email);
                    $request
                        ->getSession()
                        ->getFlashBag()
                        ->add('sonata_flash_success', 'Success. Please, check your email to continue.')
                    ;

                    return $this->redirect($this->generateUrl('exc_excel_info_page'));
                }
            }
        }

        return $this->render('ExcExcelBundle:Recovery:reset_password.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'form'            => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function forgotPasswordAction(Request $request)
    {
        if (empty($email) || empty($resetToken)) {
            throw $this->createNotFoundException('Page not found.');
        }

        $form = $this->getForgotPassForm();

        if ($request->isMethod('POST')) {
            $email = $request->query->get('e');
            $resetToken = $request->query->get('t');

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $newPassword = $data['password'];
                $count = $this->getDoctrine()->getRepository('ExcCommandBundle:Employee')->checkEmployee($email);
                if ($count) {
                    return $this->setForgotPassword($request, $email, $resetToken, $newPassword);
                }
            }
        }


        return $this->render('ExcExcelBundle:Recovery:forgot_password.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'form'            => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function newPasswordAction(Request $request)
    {
        $resetToken = $request->query->get('t');
        $form = $this->getNewPassForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $newPassword = $data['password'];
                $usr= $this->get('security.context')->getToken()->getUser();
                $id = $usr->getId();
                $employee = $this->getDoctrine()->getRepository('ExcCommandBundle:Employee')->find($id);
                if ($employee) {
                    return $this->setNewPassword($request, $resetToken, $newPassword);
                }
            }
        }
        
        return $this->render('ExcExcelBundle:Recovery:new_password.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'form'            => $form->createView()
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function infoAction()
    {
        return $this->render('ExcExcelBundle:Recovery:info_page.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
        ));
    }

    /**
     * @param Request $request
     * @param $email
     * @param $resetToken
     * @param $newPassword
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function setNewPassword(Request $request, $resetToken, $newPassword)
    {
        $usr= $this->get('security.context')->getToken()->getUser();
        $id = $usr->getId();

        $em = $this->getDoctrine()->getManager();
        $employee = $em->getRepository('ExcCommandBundle:Employee')->find($id);

        if ($this->checkResetToken($employee, $resetToken)) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($employee);
            $password = $encoder->encodePassword($newPassword, $employee->getSalt());
            $employee->setPassword($password);
            $employee->setPasswordChangedAt(null);
            $employee->setPasswordResetToken(null);
            $em->flush();
        } else {
            $request->getSession()->getFlashBag()->add('sonata_flash_error', 'Failure.');

            return $this->redirect($this->generateUrl('exc_excel_info_page'));
        }

        $request->getSession()->getFlashBag()->add('sonata_flash_success', 'Success. Your password changed.');

        return $this->redirectToLogin($employee);
    }

    /**
     * @param Request $request
     * @param $email
     * @param $resetToken
     * @param $newPassword
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function setForgotPassword(Request $request, $email, $resetToken, $newPassword)
    {
        $em = $this->getDoctrine()->getManager();
        $employee = $em->getRepository('ExcCommandBundle:Employee')->loadUserByUsername($email);

        if ($this->checkTimeDifference($employee) && $this->checkResetToken($employee, $resetToken)) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($employee);
            $password = $encoder->encodePassword($newPassword, $employee->getSalt());
            $employee->setPassword($password);
            $employee->setPasswordChangedAt(null);
            $employee->setPasswordResetToken(null);
            $em->flush();
        } else {
            $request->getSession()->getFlashBag()->add('sonata_flash_error', 'Failure.');

            return $this->redirect($this->generateUrl('exc_excel_info_page'));
        }

        $request->getSession()->getFlashBag()->add('sonata_flash_success', 'Success. Your password changed.');

        return $this->redirectToLogin($employee);

    }

    /**
     * @param string $email
     */
    protected function setPassChangedtAt($email)
    {
        $em = $this->getDoctrine()->getManager();
        $resetToken  = md5(uniqid());

        $employee = $em->getRepository('ExcCommandBundle:Employee')->loadUserByUsername($email);
        $employee->setPasswordResetToken($resetToken);

        $em->flush();

        $mailer = $this->get('exc_command.mailer.mailer');
        $mailer->sendResetPassEmail($email, $this->getResetPassLink($email, $resetToken));
    }

    /**
     * @param $email
     * @param $resetToken
     * @return string
     */
    protected function getResetPassLink($email, $resetToken)
    {
        $url = $this->generateUrl('exc_excel_forgot_password', array(
            'e' => $email,
            't' => $resetToken
        ), UrlGeneratorInterface::ABSOLUTE_URL);

        return $url;
    }

    /**
     * @param Employee $employee
     * @param $resetToken
     * @return bool
     */
    protected function checkResetToken(Employee $employee, $resetToken)
    {
        if ($resetToken == $employee->getPasswordResetToken()) {
            return true;
        }

        return false;
    }

    /**
     * @param Employee $employee
     * @return bool
     */
    protected function checkTimeDifference(Employee $employee)
    {
        $currentDate = new \DateTime();
        $diff = $employee->getPasswordChangedAt()->diff($currentDate);

        if ($diff->days <= 0 && $diff->h <= 0 && $diff->i <= 30) {
            return true;
        }

        return false;
    }


    /**
     * @param Employee $employee
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectToLogin(Employee $employee)
    {
        if ($employee->getIsManager()) {

            return $this->redirect($this->generateUrl('exc_excel_admin_login'));
        }

        return $this->redirect($this->generateUrl('exc_excel_account_login'));
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getResetPassForm()
    {
        return $this->createFormBuilder()
            ->add('email', 'text', array(
                    'label' => 'Email',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Email'),
                    'constraints' => array(
                        new NotBlank(),
                        new Email()
                    ),
                )
            )
            ->add('Reset', 'submit', array(
                'attr' => array(
                    'class' => 'btn btn-sm btn-success'
                ),
            ))
            ->getForm()
            ;
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getNewPassForm()
    {
        return $this->createFormBuilder()
            ->add('oldPassword', 'password', array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Old Password',
                    ),
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(),
                    ),
                )
            )
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'form-control')),
                'required' => true,
                'first_options'  => array('label' => 'Password', 'attr' => array('placeholder' => 'Password', 'class' => 'form-control')),
                'second_options' => array('label' => 'Repeat Password', 'attr' => array('placeholder' => 'Repeat Password', 'class' => 'form-control')),
            ))
            ->add('Change password', 'submit', array(
                'attr' => array(
                    'class' => 'btn btn-sm btn-success'
                ),
            ))
            ->getForm()
            ;
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getForgotPassForm()
    {
        return $this->createFormBuilder()
            ->add('password', 'password', array(
                    'label' => 'New Password',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Password'),
                    'constraints' => array(
                        new NotBlank(),
                    ),
                )
            )
            ->add('Apply', 'submit', array(
                'attr' => array(
                    'class' => 'btn btn-sm btn-success'
                ),
            ))
            ->getForm()
            ;
    }
} 