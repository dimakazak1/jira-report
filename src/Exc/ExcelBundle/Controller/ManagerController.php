<?php
namespace Exc\ExcelBundle\Controller;

use Exc\CommandBundle\Entity\TimeOff;
use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Form\Type\TimeOff\AddTimeOffType;
use Sonata\AdminBundle\Controller\CoreController;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

class ManagerController extends CoreController
{

    public function pageAction() {

        return $this->render('ExcExcelBundle:Account:manager.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')
        ));

    }

}