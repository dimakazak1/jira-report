<?php

namespace Exc\ExcelBundle\Controller;

use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Entity\TimeOff;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        if (true == $this->get('security.context')->isGranted(Employee::ROLE_MANAGER)
                || true == $this->get('security.context')->isGranted(Employee::ROLE_ADMIN)) {
            return $this->redirectToHomePageAction();
        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render(
            'ExcExcelBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error ,
            )
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToHomePageAction()
    {
        $userRole = $this->getUser()->getRole();

        switch ($userRole)
        {
            case Employee::ROLE_ADMIN:
                return $this->redirect($this->generateUrl('exc_admin_dashboard'));

            case Employee::ROLE_MANAGER:
                return $this->redirect($this->generateUrl('exc_excel_manager_page'));

            case Employee::ROLE_USER:
                return $this->redirect($this->generateUrl('exc_excel_account_timeoff_list'));

            default:
                return $this->redirect($this->generateUrl('exc_excel_admin_login'));
        }
    }
}
