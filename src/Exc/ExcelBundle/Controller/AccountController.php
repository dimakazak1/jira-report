<?php
namespace Exc\ExcelBundle\Controller;

use Exc\CommandBundle\Entity\TimeOff;
use Exc\CommandBundle\Entity\TypeTimeOff;
use Exc\CommandBundle\Entity\Employee;
use Exc\CommandBundle\Form\Type\TimeOff\AddTimeOffType;
use Sonata\AdminBundle\Controller\CoreController;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

class AccountController extends CoreController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        if (true == $this->get('security.context')->isGranted(Employee::ROLE_USER)) {
            return $this->redirect($this->generateUrl('exc_excel_account_timeoff_list'));
        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render(
            'ExcExcelBundle:Account:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error ,
            )
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function timeOffAction(Request $request)
    {
        if (true == $this->get('security.context')->isGranted(Employee::ROLE_MANAGER)) {
            return $this->redirect($this->generateUrl('admin_exc_command_timeoff_create'));
        }

        $timeOff = new TimeOff();

        $timeOff->setEmployee($this->getUser());

        $form = $this->createForm( new AddTimeOffType(), $timeOff);

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $em->persist($timeOff);
                $em->flush();

                $request->getSession()->getFlashBag()->add('sonata_flash_success', 'Time off successfully created.');

                return $this->redirect($this->generateUrl('exc_excel_account_timeoff_list'));
            }
        }

        return $this->render('ExcExcelBundle:Account:add_timeoff_form.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'form'            => $form->createView(),
            'time_off_type'   => $this->getDoctrine()->getRepository('ExcCommandBundle:TypeTimeOff')->getTypeTimeOffArray()
        ));
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function timeOffListAction(Request $request)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $this->getDoctrine()->getRepository('ExcCommandBundle:TimeOff')->getTimeOffListQuery($this->getUser()->getId()),
            $request->query->getInt('page', 1),
            TimeOff::DEFAULT_LIMIT
        );

        return $this->render('ExcExcelBundle:Account:timeoff_list.html.twig', array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'pagination'      => $pagination,
        ));
    }

    /**
     * @return null
     */
    protected function getTimeOffList()
    {
        return $this->getDoctrine()->getRepository('ExcCommandBundle:TimeOff')->getTimeOffList($this->getUser()->getId());
    }
}