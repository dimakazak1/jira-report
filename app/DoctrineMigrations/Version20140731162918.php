<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140731162918 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE time_off ADD CONSTRAINT FK_F13D36298C03F15C FOREIGN KEY (employee_id) REFERENCES employees (id)");
        $this->addSql("CREATE INDEX IDX_F13D36298C03F15C ON time_off (employee_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE time_off DROP FOREIGN KEY FK_F13D36298C03F15C");
        $this->addSql("DROP INDEX IDX_F13D36298C03F15C ON time_off");
    }
}
