<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150527132652 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE time_off ADD CONSTRAINT FK_F13D3629783E3463 FOREIGN KEY (manager_id) REFERENCES employee (id)");
        $this->addSql("CREATE INDEX IDX_F13D3629783E3463 ON time_off (manager_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE time_off DROP FOREIGN KEY FK_F13D3629783E3463");
        $this->addSql("DROP INDEX IDX_F13D3629783E3463 ON time_off");
    }
}
